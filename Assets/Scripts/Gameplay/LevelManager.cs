﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public float score;
    public float scoreToWin;
    public Text scoreText;

    private bool isRunning = false;
    public GameObject winModals;

    public int Level;

    public BoatMovement boat;
    public GroundSpawner ground;
    private void Start()
    {
        GetComponent<PlayerProgress>();
        score = 0;
        winModals.SetActive(false);
        isRunning = true;
    }

    private void Update()
    {
        if(isRunning)
        {
            score = score + 2 * Time.deltaTime;
            int myInt = Convert.ToInt32(score);
            scoreText.text = "Score : " + myInt;
            if (score >= scoreToWin)
            {
                isRunning = false;
                ground.isRunning = false;
                boat.isRunning = false;
                winModals.SetActive(true);
            }
        }
    }

    public void LevelCleared()
    {
        PlayerProgress.levelCleared[Level] = true;
        SceneManager.LoadScene("LevelScene");
    }
}
