﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class BoatMovement : MonoBehaviour
{
    [Header("General Attribute")]
    public Rigidbody rb;
    public float speed;
    public float horizontalMultiplier;
    public float smooth;
    public float tiltAngle;

    [Header("Sine Attribute")]
    public float magnitude;
    public float sineMultiplier;
    public float sineValue;

    float horizontalInput;
    float tiltAround;

    [Header("SwipeControl")]
    private Vector2 fingerDown;
    private Vector2 fingerUp;
    public bool detectSwipeOnlyAfterRelease = false;

    public float SWIPE_THRESHOLD = 20f;

    [Header("Border Left Right")]
    public float minX;
    public float maxX;

    public bool isRunning;
    
    private AudioSource river;

    public GameObject particle;
    public LevelManager levelManager;

    [Header("Attack")]
    private float waitTime;
    private float startWaitTime;
    public GameObject projetiles;
    void Start()
    {
        startWaitTime = 0.5f;
        waitTime = 0;
        
        isRunning = true;
        rb = GetComponent<Rigidbody>();
        river = GameObject.Find("WaterSFX").GetComponent<AudioSource>();
        StartCoroutine(RiverSfxPlay());
    }

    void Update()
    {
        if (isRunning)
        {
            if (this.transform.position.x >= maxX)
            {
                this.transform.position = new Vector3(maxX, this.transform.position.y, this.transform.position.z);
            }
            if (this.transform.position.x <= minX)
            {
                this.transform.position = new Vector3(minX, this.transform.position.y, this.transform.position.z);

            }

            //-----------SWIPE SWIPE----------------//
            SineMovement();

            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    fingerUp = touch.position;
                    fingerDown = touch.position;
                }

                //Detects Swipe while finger is still moving
                if (touch.phase == TouchPhase.Moved)
                {
                    if (!detectSwipeOnlyAfterRelease)
                    {
                        fingerDown = touch.position;
                        checkSwipe();
                    }
                }

                //Detects swipe after finger is released
                if (touch.phase == TouchPhase.Ended)
                {
                    fingerDown = touch.position;
                    horizontalInput = 0;
                    tiltAround = horizontalInput * tiltAngle;
                    checkSwipe();
                }
            }
        }

    }

    IEnumerator RiverSfxPlay()
    {
        river.Play();
        yield return new WaitForSeconds(30000);
        StartCoroutine(RiverSfxPlay());
    }

    void checkSwipe()
    {
        if (horizontalValMove() > SWIPE_THRESHOLD && horizontalValMove() > verticalMove())
        {
            horizontalInput = (this.fingerDown.x - this.fingerUp.x) * 0.02f;
            tiltAround = horizontalInput * tiltAngle;
            fingerUp = fingerDown;
        }
    }

    float verticalMove()
    {
        return Mathf.Abs(fingerDown.y - fingerUp.y);
    }

    float horizontalValMove()
    {
        return Mathf.Abs(fingerDown.x - fingerUp.x);
    }

    //-----------END SWIPE----------------//
    void FixedUpdate()
    {
        LeftRightMovement();
        RotationMovement();
    }

    void LeftRightMovement()
    {
        Vector3 forwardMove = transform.forward * speed * Time.fixedDeltaTime;
        Vector3 horizontalMove = transform.right * horizontalInput * speed * horizontalMultiplier * Time.fixedDeltaTime;
        rb.MovePosition(new Vector3(rb.position.x, sineValue, rb.position.z) + forwardMove + horizontalMove);
    }

    void RotationMovement()
    {
        Quaternion target = Quaternion.Euler(0, tiltAround, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
    }

    void SineMovement()
    {
        sineValue = sineMultiplier * Mathf.Sin(magnitude * Time.timeSinceLevelLoad);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Rock"))
        {
            levelManager.GetComponent<LevelManager>().score -= 1;
            Instantiate(particle, other.transform.position, Quaternion.identity);
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Coin"))
        {
            levelManager.GetComponent<LevelManager>().score += 1;
            Instantiate(particle, other.transform.position, Quaternion.identity);
            Destroy(other.gameObject);
        }
    }

    public void FindClosestEnemy()
    {
        float distanceToClosestEnemy = 40;
        EnemyAttack[] allEnemies = GameObject.FindObjectsOfType<EnemyAttack>();

        foreach (EnemyAttack currentEnemy in allEnemies)
        {
            if (Vector2.Distance(currentEnemy.transform.position, this.transform.position) <= distanceToClosestEnemy)
            {
                if (waitTime <= 0)
                {
                    Instantiate(projetiles, this.transform.position, Quaternion.identity);
                    waitTime = startWaitTime;
                }

                else
                {
                    waitTime -= Time.deltaTime;
                }
            }
        }
    }
}
