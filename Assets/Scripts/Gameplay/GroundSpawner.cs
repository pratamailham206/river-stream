﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSpawner : MonoBehaviour
{
    [Header("General Attribute")]
    public GameObject[] groundTile;
    public Vector3 nextSpawnPoint;
    public int totalSpawner;
    public bool isRunning;

    void Start()
    {
        isRunning = true;
        for (int i = 0; i < totalSpawner; i++)
        {
            SpawnTile();
        }
    }

    public void SpawnTile()
    {
        if (isRunning)
        {
            int RandomGround = Random.Range(0, groundTile.Length-1);
            GameObject temp = Instantiate(groundTile[RandomGround], nextSpawnPoint, Quaternion.identity);
            temp.transform.parent = this.transform;
            nextSpawnPoint = temp.transform.GetChild(1).transform.position;
        }
    }
}
