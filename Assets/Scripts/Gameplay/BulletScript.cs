﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float delayValue;
    public float bulletSpeed;
    public Vector3 firstPosition;

    private GameObject levelManager;
    public GameObject particle;

    void Start()
    {
        levelManager = GameObject.FindGameObjectWithTag("levelManager");
        firstPosition = GameObject.FindGameObjectWithTag("Boat").transform.position;
        StartCoroutine(destroyDelay());
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, firstPosition, bulletSpeed);

        if (transform.position == firstPosition)
            Destroy(gameObject);
    }

    IEnumerator destroyDelay()
    {
        yield return new WaitForSeconds(delayValue);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boat") {
            levelManager.GetComponent<LevelManager>().score -= 1;
            Destroy(gameObject);
            Instantiate(particle, other.transform.position, Quaternion.identity);
        }
    }
}
