﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [Header("General Attribute")]
    public GameObject bullet;
    public float delayShoot;
    GameObject boat;
    public bool isShooting;

    private AudioSource audioSFX;

    void Start()
    {
        isShooting = true;
        boat = GameObject.FindGameObjectWithTag("Boat");
        audioSFX = GameObject.Find("ShootSFX").GetComponent<AudioSource>();
    }

    void Update()
    {
        if(Vector3.Distance(this.transform.position, boat.transform.position) < 30f && isShooting)
        {
            isShooting = false;
            audioSFX.Play();
            StartCoroutine(Shoot());
        }
    }

    IEnumerator Shoot()
    {
        GameObject bul = Instantiate(bullet, transform.position, Quaternion.identity);
        bul.transform.parent = this.transform;

        yield return new WaitForSeconds(delayShoot);
        isShooting = true;
    }
}
