﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjecttile : MonoBehaviour
{
    public float delayValue;
    public float bulletSpeed;
    public Vector3 firstPosition;

    public GameObject particles;

    void Start()
    {
        firstPosition = GameObject.FindGameObjectWithTag("Enemy").transform.position;
        StartCoroutine(destroyDelay());
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(this.transform.position, firstPosition) <= 30)
        {
            transform.position = Vector3.MoveTowards(transform.position, firstPosition, bulletSpeed);
        }

        if (transform.position == firstPosition)
        {
            GameObject enemy = GameObject.FindGameObjectWithTag("Enemy");
            Instantiate(particles, enemy.transform.position, Quaternion.identity);
            DestroyProjectile();
            Destroy(enemy);
            Destroy(gameObject);
        }

    }
    IEnumerator destroyDelay()
    {
        yield return new WaitForSeconds(delayValue);
        Destroy(gameObject);
        
    }

    void DestroyProjectile()
    {
        Destroy(gameObject);
    }
}
