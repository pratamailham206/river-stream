﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundTile : MonoBehaviour
{
    [Header("Rock Attribute")]
    public List<GameObject> rockPrefabs;
    public int randomRock;

    [Header("Rock Attribute")]
    public List<GameObject> enemyPrefabs;
    public List<GameObject> enemySpawner;
    public int randomEnemy;
    public int randomSpawner;

    [Header("Coin Attribute")]
    public GameObject coinPrefabs;
    public Vector3 maxSpawn;
    public Vector3 targetSpawn;
    public float limitPlus;

    GroundSpawner groundSpawner;

    void Start()
    {
        groundSpawner = GameObject.FindObjectOfType<GroundSpawner>();
        
        SpawnObstacle();
        SpawnEnemy();
        SpawnCoin();
    }

    void Update()
    {
        
    }

    private void OnTriggerExit(Collider other)
    {
        groundSpawner.SpawnTile();
        Destroy(gameObject, 2);
    }

    void SpawnObstacle()
    {
        int obstacleSpawnIndex = Random.Range(2, 5);
        Transform spawnpoint = transform.GetChild(obstacleSpawnIndex).transform;

        randomRock = Random.Range(0, rockPrefabs.Count);
        if(randomRock != rockPrefabs.Count + 1)
        {
            Instantiate(rockPrefabs[randomRock], spawnpoint.position, Quaternion.identity, transform);
        }
    }

    void SpawnEnemy()
    {
        randomEnemy = Random.Range(0, enemyPrefabs.Count);
        randomSpawner = Random.Range(0, 4);

        if (randomSpawner == 0 || randomSpawner == 1)
        {
            var en = Instantiate(enemyPrefabs[randomEnemy], enemySpawner[randomSpawner].transform.position, Quaternion.identity);
            en.transform.parent = this.transform;
        }
    }

    void SpawnCoin()
    {
        maxSpawn = transform.position;
        targetSpawn = new Vector3(Random.Range(maxSpawn.x - limitPlus, maxSpawn.x + limitPlus), 2.5f, Random.Range(maxSpawn.z - limitPlus, maxSpawn.z + limitPlus));
        var coin = Instantiate(coinPrefabs, targetSpawn, Quaternion.identity);
        coin.transform.parent = transform;
    }
}
