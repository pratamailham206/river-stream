﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public string SceneName;
    public GameObject exitModals;
    public GameObject loadingScreen;

    public Text progressText;
    private void Start()
    {
        exitModals.SetActive(false);
        loadingScreen.SetActive(false);
    }

    public void MoveToScene()
    {
        StartCoroutine(LoadAsyncScene(SceneName));
    }

    public void CreditScene()
    {
        SceneManager.LoadScene("Credit");
    }

    public void MainMenuScene()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void CancelQuit()
    {
        exitModals.SetActive(false);
    }

    public void QuitGameShow()
    {
        exitModals.SetActive(true);
    }

    IEnumerator LoadAsyncScene(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
        loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            progressText.text = progress * 100f + "%";

            yield return null;
        }
    }
}
