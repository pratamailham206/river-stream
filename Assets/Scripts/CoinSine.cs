﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSine : MonoBehaviour
{
    [Header("Sine Attribute")]
    public Rigidbody rb;
    public float sineValue;
    public float sineMultiplier;
    public float magnitude;

    [Header("Rotate Attribute")]
    public Vector3 angle;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        //SineMovement();
        RotateMovement();
    }

    void SineMovement()
    {
        sineValue = sineMultiplier * Mathf.Sin(magnitude * Time.timeSinceLevelLoad);
        rb.MovePosition(new Vector3(rb.position.x, sineValue, rb.position.z));
    }

    void RotateMovement()
    {
        transform.Rotate(angle.x, angle.y, angle.z, Space.Self);
    }
}
