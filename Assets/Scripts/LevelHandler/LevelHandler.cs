﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelHandler : MonoBehaviour
{
    public GameObject[] Level;
    public Sprite unlocked;
    public Sprite locked;

    public GameObject loadingScreen;
    public Text progressText;

    void Start()
    {
        GetComponent<PlayerProgress>();
        for (int i = 0; i < Level.Length; i++)
        {
            Level[i].GetComponent<Image>().sprite = locked;
            Level[i].GetComponent<Button>().enabled = false;
        }
    }

    void Update()
    {        
        for (int i = 0; i < PlayerProgress.levelCleared.Length; i++)
        {
            if(PlayerProgress.levelCleared[i] == true)
            {
                Level[i].GetComponent<Image>().sprite = unlocked;
                Level[i].transform.GetChild(0).gameObject.SetActive(true);
                Level[i].GetComponent<Button>().enabled = true;
            }
        }
    }

    public void MoveToLevel1()
    {
        StartCoroutine(LoadAsyncScene("GameScene"));
    }

    public void MoveToLevel2()
    {
        StartCoroutine(LoadAsyncScene("GameScene1"));
    }

    public void MoveToLevel3()
    {
        StartCoroutine(LoadAsyncScene("GameScene2"));
    }

    public void MoveToLevel4()
    {
        StartCoroutine(LoadAsyncScene("GameScene3"));
    }

    public void MoveToLevel5()
    {
        StartCoroutine(LoadAsyncScene("GameScene4"));
    }

    public void MoveToLevel6()
    {
        StartCoroutine(LoadAsyncScene("GameScene6"));
    }


    public void MoveToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    IEnumerator LoadAsyncScene(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
        loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            progressText.text = progress * 100f + "%";

            yield return null;
        }
    }
}
