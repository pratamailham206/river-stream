﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProgress : MonoBehaviour
{
    // Start is called before the first frame update
    public static bool[] levelCleared;

    void Start()
    {
        levelCleared = new bool[6];
        levelCleared[0] = true;
        levelCleared[1] = true;
        levelCleared[2] = true;
        levelCleared[3] = true;
        levelCleared[4] = true;
        levelCleared[5] = true;
    }
}
